Place self-contained JavaScript source files in this directory.

IMPORTANT: Files or libraries that are already minified should be placed
directly in the top-level js directory, NOT src/js.
